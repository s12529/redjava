<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<head>
<title>Home</title>
</head>
<t:layout>

	<jsp:body>
		<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item"
							src="http://www.youtube.com/embed/i__1Z5a9Sak" allowfullscreen=""></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
	</jsp:body>
	
</t:layout>