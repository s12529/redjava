<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<head>
<title>Edit Profile</title>
</head>
<t:layout>

	<jsp:body>
	<div class="section section-danger">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisici elit, <br>sed
						eiusmod tempor incidunt ut labore et dolore magna aliqua.<br>
						Ut enim ad minim veniam, quis nostrud
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<form role="form">
						<div class="form-group">
							<label class="control-label">Image</label><input type="file">
						</div>
						<div class="form-group">
							<label class="control-label" for="exampleInputEmail1">Age</label>
							<input class="form-control" id="exampleInputEmail1"
								placeholder="Enter age" type="text">
						</div>
						<div class="form-group">
							<label class="control-label">City</label><input
								class="form-control" type="text" placeholder="Enter city">
						</div>
						<div class="form-group">
							<label class="control-label" for="exampleInputEmail1">Weight</label>
							<input class="form-control" id="exampleInputEmail1"
								placeholder="Enter weight" type="text">
						</div>
						<div class="form-group">
							<label class="control-label" for="exampleInputPassword1">Eye
								color</label> <input class="form-control" id="exampleInputPassword1"
								placeholder="Enter eye color" type="text">
						</div>

						<div class="form-group">
							<label class="control-label">Hair color<br></label><input
								class="form-control" type="text" placeholder="Enter hair color">
						</div>
						<div class="form-group">
							<label class="control-label">Education<br></label><select
								class="form-control">
								<option>Podstawowe</option>
								<option>Srednie</option>
								<option>Wyzsze</option>
							</select>
						</div>
						<button type="submit" class="btn btn-default">Submit</button>
					</form>
				</div>
				<div class="col-md-6">
					<div class="thumbnail">
						<img
							src="https://unsplash.imgix.net/photo-1416400453940-65c69d70ad91?w=1024&amp;q=50&amp;fm=jpg&amp;s=54806c1456b35f1d56ca356663d1bdd2"
							class="img-responsive">
						<div class="caption">
							<h3>Image label</h3>
							<p>Cras justo odio, dapibus ac facilisis in, egestas eget
								quam. Donec id elit non mi porta gravida at eget metus. Nullam
								id dolor id nibh ultricies vehicula ut id elit.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

		</jsp:body>
</t:layout>