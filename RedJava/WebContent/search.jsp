<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<head>
<title>Search</title>
</head>
<t:layout>
	<jsp:body>
	<div class="section section-danger">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisici elit, <br>sed
						eiusmod tempor incidunt ut labore et dolore magna aliqua. <br>
						Ut enim ad minim veniam, quis nostrud
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="section">

		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<form role="form">
						<div class="form-group has-feedback"></div>

						<label class="control-label" for="sizing-addon1">Age</label>
						<div class="input-group">
							<span class="input-group-addon" id="sizing-addon1">from</span> <input
								type="text" class="form-control"
								aria-describedby="sizing-addon1">
						</div>


						<div class="input-group">
							<span class="input-group-addon" id="sizing-addon2">to</span> <input
								type="text" class="form-control"
								aria-describedby="sizing-addon2">
						</div>

						<div class="form-group">
							<label class="control-label">Education<br></label><select
								class="form-control"><option>Podstawowe</option>
								<option>Srednie</option>
								<option>Wyzsze</option></select>
						</div>
						<div class="form-group">
							<label class="control-label">Weight</label><input
								class="form-control" type="text">
						</div>
						<div class="form-group">
							<label class="control-label">Height</label><input
								class="form-control" type="text">
						</div>
						<div class="form-group">
							<label class="control-label">City</label><input
								class="form-control" type="text">
						</div>
						<button type="submit" class="btn btn-default">Search</button>
					</form>
				</div>
				<div class="col-md-3">
					<div class="well">
						<a href="#"><img
							src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png"
							class="img-responsive"></a>
						<h2>Name 1</h2>
						<p>Donec id elit non mi porta gravida at eget metus. Fusce
							dapibus, tellus ac cursus commodo, tortor mauris condimentum
							nibh, ut fermentum massa justo sit amet risus. Etiam porta sem
							malesuada magna mollis euismod. Donec sed odio dui.</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="well">
						<a href="#"><img
							src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png"
							class="img-responsive"></a>
						<h2>Name 2</h2>
						<p>Donec id elit non mi porta gravida at eget metus. Fusce
							dapibus, tellus ac cursus commodo, tortor mauris condimentum
							nibh, ut fermentum massa justo sit amet risus. Etiam porta sem
							malesuada magna mollis euismod. Donec sed odio dui.</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="well">
						<a href="#"><img
							src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png"
							class="img-responsive"></a>
						<h2>Name 3</h2>
						<p>Donec id elit non mi porta gravida at eget metus. Fusce
							dapibus, tellus ac cursus commodo, tortor mauris condimentum
							nibh, ut fermentum massa justo sit amet risus. Etiam porta sem
							malesuada magna mollis euismod. Donec sed odio dui.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
		</jsp:body>
</t:layout>