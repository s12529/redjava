<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<head>
<title>Friends</title>
</head>
<t:layout>

	<jsp:body>
	<div class="section section-danger">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisici elit, <br>sed
						eiusmod tempor incidunt ut labore et dolore magna aliqua.<br>
						Ut enim ad minim veniam, quis nostrud
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<a href="#"> <img
						src="http://pingendo.github.io/pingendo-bootstrap/assets/blurry/800x600/1.jpg"
						class="center-block img-responsive"></a>
				</div>
				<div class="col-md-9">
					<div class="well">
						<h2>Name 1</h2>
						<p>Donec id elit non mi porta gravida at eget metus. Fusce
							dapibus, tellus ac cursus commodo, tortor mauris condimentum
							nibh, ut fermentum massa justo sit amet risus. Etiam porta sem
							malesuada magna mollis euismod. Donec sed odio dui.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<a href="#"> <img
						src="http://pingendo.github.io/pingendo-bootstrap/assets/blurry/800x600/1.jpg"
						class="center-block img-responsive"></a>
				</div>
				<div class="col-md-9">
					<div class="well">
						<h2>Name 2</h2>
						<p>Donec id elit non mi porta gravida at eget metus. Fusce
							dapibus, tellus ac cursus commodo, tortor mauris condimentum
							nibh, ut fermentum massa justo sit amet risus. Etiam porta sem
							malesuada magna mollis euismod. Donec sed odio dui.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<a href="#"><img
						src="http://pingendo.github.io/pingendo-bootstrap/assets/blurry/800x600/1.jpg"
						class="center-block img-responsive"></a>
				</div>
				<div class="col-md-9">
					<div class="well">
						<h2>Name 3</h2>
						<p>Donec id elit non mi porta gravida at eget metus. Fusce
							dapibus, tellus ac cursus commodo, tortor mauris condimentum
							nibh, ut fermentum massa justo sit amet risus. Etiam porta sem
							malesuada magna mollis euismod. Donec sed odio dui.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
		</jsp:body>
</t:layout>