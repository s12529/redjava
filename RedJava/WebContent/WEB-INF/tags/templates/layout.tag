<%@ tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="styles" fragment="true"%>
<%@attribute name="menu" fragment="true"%>
<%@attribute name="footer" fragment="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
  <%@include file = "../../../bootstrap/bootstrap.html"  %>
  <jsp:invoke fragment="styles"/>
</head>
<body>
	<div id="menu">
		<div class="section section-primary">
			<div class="container">
				<div class="row">
					<div class="col-md-11">

						<h1>
							<ul class="list-inline">
								<li><a href="index.jsp"><img
										src="//placehold.it/150x50"></a></li>
								<li><p>
									<h1>RedJava.pl</h1>
									</p></li>
							</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="section text-right">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="lead nav nav-pills">
							<li class="active"><a href="profile.jsp">Profile</a></li>
							<li class=""><a href="friends.jsp">Friends</a></li>
							<li><a href="#">Visitors</a></li>
							<li><a href="search.jsp">Search</a></li>
							<li><a href="#">Logout</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		  <jsp:invoke fragment="menu"/>
	</div>

    <div id="body">
      <jsp:doBody/>
    </div>

	<div id="footer">	
	<footer class="section section-primary">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<h1>Footer header</h1>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisici elit, <br>sed
					eiusmod tempor incidunt ut labore et dolore magna aliqua. <br>Ut
					enim ad minim veniam, quis nostrud
				</p>
			</div>
			<div class="col-sm-6">
				<p class="text-info text-right">
					<br>
					<br>
				</p>
				<div class="row">
					<div class="col-md-12 hidden-lg hidden-md hidden-sm text-left">
						<a href="#"><i
							class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a> <a href="#"><i
							class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a> <a href="#"><i
							class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a> <a href="#"><i
							class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 hidden-xs text-right">
						<a href="#"><i
							class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a> <a href="#"><i
							class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a> <a href="#"><i
							class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a> <a href="#"><i
							class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
		  <jsp:invoke fragment="footer"/>
</div>

</body>
</html>
