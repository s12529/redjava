<%@ page language="java" contentType="text/html"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>    
<head>
<title>Profile</title>
</head>
<t:layout>
	<jsp:body>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<i class="fa fa-3x fa-child fa-fw pull-left"></i>
					<h3 class="text-left">Witaj null!</h3>
					<div class="thumbnail">
						<img
							src="https://unsplash.imgix.net/photo-1416400453940-65c69d70ad91?w=1024&amp;q=50&amp;fm=jpg&amp;s=54806c1456b35f1d56ca356663d1bdd2"
							class="img-responsive">
					</div>

					<a href="editProfile.jsp" class="btn btn-block btn-primary"
						role="button">Edit Profile</a>

				</div>
				<div class="col-md-9">
					<ul class="media-list">

						<li class="media"><a class="pull-left"><img
								class="media-object"
								src="http://pingendo.github.io/pingendo-bootstrap/assets/blurry/800x600/12.jpg"
								height="64" width="64"></a>
						<div class="media-body">
								<h4 class="media-heading">About me</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel
									metus scelerisque ante sollicitudin commodo. Cras purus odio,
									vestibulum in vulputate at, tempus viverra turpis.</p>
							</div></li>
					</ul>
					<ul class="list-group">
						<li class="list-group-item">Age</li>
						<li class="list-group-item">Weight</li>
						<li class="list-group-item">Eye color</li>
						<li class="list-group-item">Hair color</li>
						<li class="list-group-item">Education</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="well">
						<h2>Who I am looking for?</h2>
						<p>Donec id elit non mi porta gravida at eget metus. Fusce
							dapibus, tellus ac cursus commodo, tortor mauris condimentum
							nibh, ut fermentum massa justo sit amet risus. Etiam porta sem
							malesuada magna mollis euismod. Donec sed odio dui.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

		</jsp:body>
</t:layout>